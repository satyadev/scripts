Contains my .emacs file. The goal is to maintain my various experiments
over time.

I have learnt a lot from "The Emacs Lisp Tutorial" by Robert J.
Chassell. 

Magnar Sveen of "Emacs Rocks" fame is another great place for ideas. It 
is humbling to know that he picked up a lot of emacs tricks in hardly
a month or two.
