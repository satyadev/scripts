;;-----------------------------
;; Utility functions
;;-----------------------------
(defun @ ()
  "insert the current time string at point."
  (interactive) 
  (insert (current-time-string)))


;;---------------------------
;; Global key bindings
;;---------------------------
(global-set-key `[f5] `@)
(global-set-key `[select] `move-end-of-line)
(global-set-key `[f7] `sgml-close-tag)



;;---------------------
;; Default modes at start up
;;----------------------
(setq tramp-default-method "ssh")
(transient-mark-mode 1)
(auto-fill-mode)
(display-time-mode 1)
(show-paren-mode 1)

(add-hook 'text-mode-hook 'turn-on-auto-fill)

;;-------------------------------
;; Org mode specific
;;--------------------------------
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) 
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)


(require 'saveplace)
(setq-default save-place t)

(defun dos2unix (buffer)
  "Automate M-% C-q C-m RET C-q C-j RET"
  (interactive "*b")
  (save-excursion
    (goto-char (point-min))
    (while (search-forward (string ?\C-m) nil t)
      (replace-match (string ?\C-j) nil t))))

;;---------------------------------------
;; Tex mode specific
;;--------------------------------------
(setq latex-run-command "pdflatex")
(defun tex-mode-bind-key ()
  (interactive)
  (global-set-key "\C-c]" 'tex-close-latex-block))
(add-hook 'tex-mode-hook 'tex-mode-bind-key)

;;---------------------------------------
;; speeding up 
;;---------------------------------------

;; To speed up emacs, disable vc-git
;; http://stackoverflow.com/questions/5748814
;; /how-does-one-disable-vc-git-in-emacs

(require 'vc)
(remove-hook 'find-file-hook 'vc-find-file-hook)
(setq font-lock-maximum-decoration 0) ; improves speed, reduces memory
(global-font-lock-mode 1)

;;-----------------------------------
;; Occur
;;-----------------------------------
(global-set-key `[M-s o] `occur)

;;-----------------------------------
;; Narrowing specific
;;----------------------------------
(put 'narrow-to-region 'disabled nil)

(defmacro maybe (expression &optional FALLBACK)
  `(let
       ((val ,expression))
     (cond
      ((not (null val)) val)
      (t ,FALLBACK))))

(defun narrow-to-section ()
  "Narrow to the current section"
  (interactive)
  (let (start end)
    (save-excursion (progn
		      (setq start (maybe
				   (search-backward "\\section" nil t)
				   (point-min)))
		      (forward-char)
		      (setq end (maybe
				 (search-forward "\\section" nil t)
				 (point-max)))
		      (narrow-to-region start end)))))


;;-------------------------------
;; Melpa
;;------------------------------
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
